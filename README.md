# dnnHealth

ECG Data Augmentation with CNN GAN

please look at ecgGAN.ipynb

training:

![alt text](GAN_Loss_per_Epoch_final_1.png)

retraining:

![alt text](GAN_Loss_per_Epoch_final_2.png)

synthetic ECG (normal heart condition)

![alt text](result.png)

